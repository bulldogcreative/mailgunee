# MailgunEE ExpressionEngine Add-on

**This is NOT supported by Mailgun**.

## Installation

1. [Sign up](http://www.mailgun.com/) for a Mailgun account
2. [Download](https://bitbucket.org/bulldogcreative/mailgunee/downloads) the zip file
3. Create the directory **mailgunee** in **/system/expressionengine/third_party/** or your third party folder if moved your **system** folder.
4. Upload the files into the new directory
5. Login to ExpressionEngine
6. Click **Add-Ons** -> **Extensions**
7. Click **Install** on the **MailgunEE** row
8. Click **Settings** on the **MailgunEE** row
9. Enter your **API Key** and **Mailgun domain**
10. Click **Submit** and you're done

All of the email from your website will now be handled by **Mailgun**.

## Change Log

### Version 1.0.0

April 12, 2015

Initial release

## Disclaimer

If you have any problems with this add-on, do **NOT** contact Mailgun. Please create a new [issue](https://bitbucket.org/bulldogcreative/mailgunee/issues) and we will do our best to help solve it.