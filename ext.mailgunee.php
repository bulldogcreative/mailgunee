<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed.');

/**
 * Send email using Mailgun API
 *
 * @version 1.0.0
 * @author Levi Durfee <ldurfee@bulldogcreative.com>
 *
 */

// Require the Mailgun PHP
require(PATH_THIRD . "/mailgunee/libs/vendor/autoload.php");
use Mailgun\Mailgun;

class Mailgunee_ext {

    var $name = "MailgunEE";
    var $ext_class = "Mailgunee";
    var $version = "1.0.0";
    var $description = "http://www.mailgun.com/ - making email easier.";
    var $settings_exist = "y";
    var $docs_url = "https://bitbucket.org/bulldogcreative/mailgunee";

    var $settings = array();

    function __construct($settings = "")
    {
        $this->settings = $settings;
    }

    function activate_extension()
    {
        $data = array(
            "class"     => __CLASS__,
            "method"    => "sendMessage",
            "hook"      => "email_send",
            "settings"  => "",
            "priority"  => 10,
            "version"   => $this->version,
            "enabled"   => "y",
            );

        ee()->db->insert("extensions", $data);
    }

    function update_extension($current = "")
    {
        if($current == "" OR $current == $this->version)
        {
            return false;
        }

        if($current < "0.0.1")
        {
            // Update to version 1.0.0
        }

        ee()->db->where("class", __CLASS__);
        ee()->db->update("extensions", array("version" => $this->version));
    }

    function disable_extension()
    {
        ee()->db->where("class", __CLASS__);
        ee()->db->delete("extensions");
    }

    function sendMessage($data)
    {        
        $mg = new Mailgun($this->settings["apiKey"]);
        $domain = $this->settings["mgdomain"];

        $mg->sendMessage($domain, 
            array(
                "from"    => str_replace("\"", "", $data["headers"]["Reply-To"]),
                "to"      => $data["recipients"],
                "cc"      => $data["cc_array"],
                "bcc"     => $data["bcc_array"],
                "subject" => $data["subject"], 
                "text"    => $data["finalbody"],
                "o:tag"   => $data["headers"]["User-Agent"]));

        ee()->extensions->end_script = true;
        ee()->extensions->_spool_email = true;
        return ee()->extensions->end_script;
    }

    function settings_form($current)
    {
        ee()->load->helper("form");
        ee()->load->library("table");

        $vars = array();

        $mgdomain = (isset($current["mgdomain"])) ? $current["mgdomain"] : "";
        $apiKey = (isset($current["apiKey"])) ? $current["apiKey"] : "";

        $vars["settings"] = array(
            "apiKey" => form_input("apiKey", $apiKey),
            "mgdomain" => form_input("mgdomain", $mgdomain),
            );

        return ee()->load->view("index", $vars, true);
    }

    function save_settings()
    {
        if(empty($_POST))
        {
            show_error(lang("unauthorized_access"));
        }

        unset($_POST["submit"]);

        ee()->lang->loadfile("mailgunee");

        ee()->db->where("class", __CLASS__);
        ee()->db->update("extensions", array("settings" => serialize($_POST)));

        ee()->session->set_flashdata(
            "message_success",
            lang("preferences_updated")
        );
    }
}
